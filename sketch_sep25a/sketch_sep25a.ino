const int discharges [4] = {13, 12, 11, 10};
const int segments [8] = {9, 8, 7, 6, 5, 4, 3, 2};

bool sample_words [7][8] = {{0, 1, 0, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 0, 0, 1, 1}, {0, 1, 1, 0, 0, 0, 0, 1}, {1, 0, 0, 1, 1, 0, 0, 1}, {0, 0, 1, 1, 0, 0, 0, 1}, {1, 1, 1, 1, 1, 1, 1, 1}};
bool* label_sitting_time [4] = {&(sample_words[6][0]), &(sample_words[3][0]), &(sample_words[5][0]), &(sample_words[1][0])};
bool* label_timer [4] = {&(sample_words[6][0]), &(sample_words[2][0]), &(sample_words[4][0]), &(sample_words[0][0])};
bool sample_numbers [10][8] = {{0, 0, 0, 0, 0, 0, 1, 1}, {1, 0, 0, 1, 1, 1, 1, 1}, {0, 0, 1, 0, 0, 1, 0, 1}, {0, 1, 1, 0, 0, 0, 0, 1}, {1, 0, 0, 1, 1, 0, 0, 1}, {0, 1, 0, 0, 1, 0, 0, 1}, {0, 1, 0, 0, 0, 0, 0, 1}, {0, 0, 0, 1, 1, 1, 1, 1}, {0, 0, 0, 0, 0, 0, 0, 1}, {0, 0, 0, 0, 1, 0, 0, 1}};

uint32_t number_result;

bool* label_numbers[4] = {sample_numbers[0], sample_numbers[0], sample_numbers[0], sample_numbers[0]};

bool dischange;

bool menu_or_numbers = true;

extern volatile unsigned long timer0_millis;

uint32_t now_time;
uint32_t user_now_time;
int period = 1000;
uint32_t user_time = 10000;
uint32_t timer = 20000;

 
// указатель на 2ух мерный массив
bool** indication;

bool time_or_timer; 

void show_discharge(int discharge){
  digitalWrite(discharges[discharge], HIGH);
  for (int a; a < 8; a++) {
      digitalWrite(segments[a], (indication[discharge])[a]);
  }

  delay (5);

  digitalWrite(discharges[discharge], LOW);
  for (int a; a < 8; a++) {
      digitalWrite(segments[a], HIGH);
  }
}

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  
  pinMode(52, INPUT_PULLUP);
  pinMode(A0, INPUT);
  
  for (int i = 0; i < 4; i++){
    Serial.println(discharges[i]);
    pinMode(discharges[i], OUTPUT);
  }
  
  for (int i = 0; i < 8; i++){
      pinMode(segments[i], OUTPUT);
      digitalWrite(segments[i], HIGH);
  }

}

int tune_number(int number){
  String string_number = String(number);
  label_numbers[3 - int(dischange) * 2] = sample_numbers[(string_number[0] - '0')-1];
  label_numbers[2 - int(dischange) * 2] = sample_numbers[(string_number[1] - '0')];
}

int tune_numbers(){
  int number = analogRead(A0);
  if(!dischange){
    tune_number(map(number, 0, 1023, 10, 33));
    return map(number, 0, 1023, 0, 23);
  }
  else{
    tune_number(map(number, 0, 1023, 10, 69));
    return map(number, 0, 1023, 0, 59);  
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  pinMode(51, OUTPUT);
  if (menu_or_numbers) {
    if (analogRead(A0)<129){
      indication = label_sitting_time;
      time_or_timer = true;
    }
    else{
      indication = label_timer;
      time_or_timer = false;
    }
  }

  if(digitalRead(52) == 0){
    Serial.println("button");
    if (dischange){
      number_result += uint32_t(tune_numbers())*60*1000;
      Serial.println(uint32_t(tune_numbers())*1000); 
      if (time_or_timer){
         user_time = number_result - now_time;
      }
      else{
        timer = number_result;
      }
    }
    else{
      number_result = tune_numbers()*1000*3600; 
      Serial.println(number_result);
    }
    
    
    if(menu_or_numbers){
      menu_or_numbers = false;
      indication = label_numbers; 
      delay(500);
    }
    else if(dischange){
      menu_or_numbers = true;
      dischange = false;
      delay(500);
    }
    else{
      dischange = true;
      delay(500);
    }
  }

  if (!menu_or_numbers){
    tune_numbers();
  } 

  if(millis() - now_time >= period){
    now_time += period * ((millis() - now_time)/period);
    user_now_time = now_time + user_time;
    Serial.println("time");
    Serial.println(user_now_time);
    uint32_t sec = user_now_time/1000;
    if(user_now_time == timer){
      tone(51, 2000, 2000);
    }
  }

  show_discharge(0);
  show_discharge(1);
  show_discharge(2);
  show_discharge(3);
}   
