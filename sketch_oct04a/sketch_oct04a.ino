extern volatile unsigned long timer0_millis;

uint32_t now_time;
uint32_t user_now_time;
int period = 1000;
uint32_t user_time = 10000;
uint32_t timer = 20000;


void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(53, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  //uint32_t left_time = millis() - now_time;
  if(millis() - now_time >= period){
    now_time += (millis() - now_time);
    user_now_time = now_time + user_time;
    uint32_t sec = user_now_time/1000;
    Serial.print((sec/3600ul)%24);
    Serial.print(',');
    Serial.print((sec%3600ul)/60);
    Serial.print(',');
    Serial.println((sec%3600ul)%60ul);
    delay(500);
    if(user_now_time == timer){
      Serial.println("timer");
      tone(53, 5000, 1000);
    }
  }
}
